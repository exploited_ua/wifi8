
//= ../../node_modules/swiper/dist/js/swiper.jquery.js
//= jquery.ddslick.min.js
//= canvas-video-player.js


var Slider = new Swiper('.slider-block', {
    slidesPerView: '1',
    pagination: '.slider-pagination',
    paginationClickable: true,
    spaceBetween: 10,
    autoplay: 2000,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    breakpoints: {
        767: {
            slidesPerView: 'auto'
        }
    }
});

$('.scroll-downs').on('click', function () {
   var windowHeight = $(window).height();
   var scrollPosition = windowHeight * 0.6;
    $("html, body").animate({
        scrollTop: scrollPosition
    }, 600);
    return false;
});

$(document).ready(function () {

    function addScroll() {
        $('body').removeClass('video-play');
        $('.scroll-downs').addClass('active');
    }
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if(isAndroid) {
        addScroll();
    }

    var updateTime = function () {
        var timeVideoPlay = $('video')[0].currentTime;
        if(timeVideoPlay > 5){
            addScroll();
        }
    };
    $('video').bind('timeupdate', updateTime);

});



$('#myDropdown').ddslick({
    onSelected: function(selectedData){
    }
});

// open feedback modal
function openFeedbackModal() {
    $('#feedback').show();
}

$('#btn_feedback').on('click', function (e) {
    e.preventDefault();
    openFeedbackModal();
});

// close modal
function closeModal(){
    $('#conditions').hide();
    $('#feedback').hide();
    $('#succsess_feedback').hide();
}

$('.close_modal').on('click', function () {
    closeModal();
});

// open Succsess Feedback Modal

function openSuccsessFeedbackModal() {
    $('#feedback').hide();
    $('#succsess_feedback').show();
}

$('.btn_confirm').on('click', function (e) {
    e.preventDefault();
    openSuccsessFeedbackModal();
});

// open Terms and Conditions

function openConditionsModal() {
    $('#conditions').show();
}

$("#registration-form-terms-and-conditions-text").on('click', function () {
    openConditionsModal();
});

// open After Registration block
function openAfterRegistration() {
    $('.after-registration').show().css('display','flex');
}

$("#registration-form-submit-button").on('click', function (e) {
    e.preventDefault();
    openAfterRegistration();
});


